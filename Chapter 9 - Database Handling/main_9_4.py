from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem
import sqlite3
import sys
from demo_9_4 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.query = ""
        self.ui.pushButtonDisplayRows.clicked.connect(self.displayRows)
        self.show()

    def displayRows(self):
        databaseName = self.ui.lineEditDBName.text()
        tableName = self.ui.lineEditTableName.text()
        self.query = "SELECT * FROM " + tableName

        try:
            conn = sqlite3.connect(databaseName + ".db")
            self.ui.labelResponse.setText("Database connected.")

            cur = conn.cursor()
            cur.execute(self.query)
            rows = cur.fetchall()

            rowID = 0
            for row in rows:
                self.ui.labelResponse.setText("")
                colID = 0
                for col in row:
                    self.ui.tableWidget.setItem(rowID, colID, QTableWidgetItem(col))
                    colID += 1
                rowID += 1

        except:
            self.ui.tableWidget.clear()
            self.ui.labelResponse.setText("Error.")

        finally:
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
