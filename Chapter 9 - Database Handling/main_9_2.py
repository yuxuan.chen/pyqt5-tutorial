from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
import sys
from demo_9_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.request = ""

        self.ui.pushButtonAddColumn.clicked.connect(self.addColumn)
        self.ui.pushButtonCreateTable.clicked.connect(self.createTable)

        self.show()

    def addColumn(self):
        tableName = self.ui.lineEditTableName.text()
        columnName = self.ui.lineEditColumnName.text()
        dataType = self.ui.comboBoxDataType.itemText(
            self.ui.comboBoxDataType.currentIndex()
        )

        if self.request == "":
            self.request = "CREATE TABLE IF NOT EXISTS {} ({} {}".format(
                tableName, columnName, dataType
            )
        else:
            self.request += ", {} {}".format(columnName, dataType)
            self.ui.lineEditColumnName.setText("")
            self.ui.lineEditColumnName.setFocus()

    def createTable(self):
        try:
            connectSlotsByName = sqlite3.connect(self.ui.lineEditDBName.text() + ".db")
            self.ui.labelResponse.setText("Database is connected")

            cur = conn.cursor()
            self.request += ");"
            cur.execute(self.request)
            self.ui.labelResponse.setText("Table is successfully created.")

        except:
            self.ui.labelResponse.setText("Error in creating table.")

        finally:
            self.request = ""
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
