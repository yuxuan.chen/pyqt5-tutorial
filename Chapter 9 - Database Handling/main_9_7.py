from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3, sys
from demo_9_7 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonSignIn.clicked.connect(self.searchRows)
        self.show()

    def searchRows(self):
        emailAddress = self.ui.lineEditEmailAddress.text()
        password = self.ui.lineEditPassword.text()
        query = (
            "SELECT Email, Password FROM tb "
            "where Email like {} and Password like {}".format(emailAddress, password)
        )

        try:
            conn = sqlite3.connect("db.db")
            cur = conn.cursor()
            cur.execute(query)
            row = cur.fetchone()
            if row:
                self.ui.labelResponse.setText("You are welcome.")
            else:
                self.ui.labelResponse.setText(
                    "Sorry, incorrect email address or password."
                )
        except:
            self.ui.labelResponse.setText("Error.")
        finally:
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
