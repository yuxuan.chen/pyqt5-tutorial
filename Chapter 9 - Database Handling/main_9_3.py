from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
import sys
from demo_9_3 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.request = ""
        self.ui.pushButtonInsertRow.clicked.connect(self.insertRow)
        self.show()

    def insertRow(self):
        databaseName = self.ui.lineEditDBName.text()
        tableName = self.ui.lineEditTableName.text()
        emailAddress = self.ui.lineEditEmailAddress.text()
        password = self.ui.lineEditPassword.text()

        conn = sqlite3.connect(databaseName + ".db")
        self.ui.labelResponse.setText("Database is connected.")

        try:  # if the table does not exist
            conn.cursor().execute(
                "CREATE TABLE IF NOT EXISTS {} "
                "(Email TEXT, Password TEXT);".format(tableName)
            )

            self.request = (
                'INSERT INTO {} (Email, Password) VALUES ("{}", "{}");'.format(
                    tableName, emailAddress, password
                )
            )
            conn.cursor().execute(self.request)
            self.ui.labelResponse.setText("The row is successfully inserted.")

        except:
            self.ui.labelResponse.setText("Error.")

        finally:
            self.request = ""
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
