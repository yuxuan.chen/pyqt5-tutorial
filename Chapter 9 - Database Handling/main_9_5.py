from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
import sys
from demo_9_5 import *


class MyForm(QDialog):
    def __init__(self, databaseName="db.db", tableName="tb"):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.tableName = tableName
        self.query = "SELECT Email, Password FROM " + self.tableName
        self.conn = sqlite3.connect(databaseName)
        self.rowID = 1

        self.ui.pushButtonFirstRow.clicked.connect(self.displayFirstRow)
        self.ui.pushButtonPrevious.clicked.connect(self.displayPreviousRow)
        self.ui.pushButtonNext.clicked.connect(self.displayNextRow)
        self.ui.pushButtonLastRow.clicked.connect(self.displayLastRow)

        self.show()

    def displayFirstRow(self):
        try:
            self.rowID = 1
            cur = self.conn.cursor()
            cur.execute(self.query + " WHERE rowid=" + str(self.rowID) + ";")
            row = cur.fetchone()
            if row:
                self.ui.lineEditEmailAddress.setText(row[0])
                self.ui.lineEditPassword.setText(row[1])
        except:
            self.ui.labelResponse.setText("Error.")

    def displayPreviousRow(self):
        self.rowID -= 1
        cur = self.conn.cursor()
        cur.execute(self.query + " WHERE rowid=" + str(self.rowID) + ";")
        row = cur.fetchone()

        if row:
            self.ui.labelResponse.setText("")
            self.ui.lineEditEmailAddress.setText(row[0])
            self.ui.lineEditPassword.setText(row[1])
        else:
            self.rowID += 1
            self.ui.labelResponse.setText("This is the first row.")

    def displayNextRow(self):
        self.rowID += 1
        cur = self.conn.cursor()
        cur.execute(self.query + " WHERE rowid=" + str(self.rowID) + ";")
        row = cur.fetchone()

        if row:
            self.ui.labelResponse.setText("")
            self.ui.lineEditEmailAddress.setText(row[0])
            self.ui.lineEditPassword.setText(row[1])
        else:
            self.rowID -= 1
            self.ui.labelResponse.setText("This is the last row.")

    def displayLastRow(self):
        # get row count
        cur = self.conn.cursor()
        cur.execute("SELECT COUNT(*) FROM " + self.tableName + ";")
        self.rowID = cur.fetchone()[0]

        cur = self.conn.cursor()
        cur.execute(self.query + " WHERE rowid=" + str(self.rowID) + ";")
        row = cur.fetchone()
        if row:
            self.ui.lineEditEmailAddress.setText(row[0])
            self.ui.lineEditPassword.setText(row[1])


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
