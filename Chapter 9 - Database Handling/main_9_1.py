from PyQt5.QtWidgets import QDialog, QApplication
import sys, sqlite3
from demo_9_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonCreateDB.clicked.connect(self.createDB)
        self.show()

    def createDB(self):
        try:
            conn = sqlite3.connect("{}.db".format(self.ui.lineEditDBName))
            self.ui.labelResponse.setText("Database is created.")
            conn.close()
        except:
            self.ui.labelResponse.setText("Some error has occurred.")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
