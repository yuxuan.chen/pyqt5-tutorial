from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3, sys
from demo_9_8 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonChangePassword.clicked.connect(self.changePassword)
        self.show()

    def changePassword(self):
        emailAddress = self.ui.lineEditEmailAddress.text()
        oldPassword = self.ui.lineEditOldPassword.text()
        newPassword = self.ui.lineEditNewPassword.text()
        reenterPassword = self.ui.lineEditReenterNew.text()
        if newPassword != reenterPassword:
            self.ui.labelResponse.setText("Two new passwords do not match.")
            return

        # search and update
        try:
            conn = sqlite3.connect("db.db")
            cur = conn.cursor()
            cur.execute(
                "SELECT Email, Password FROM tb "
                "WHERE Email like {} and Password like {};".format(
                    emailAddress, oldPassword
                )
            )
            row = cur.fetchone()

            if row:
                cur = conn.cursor()
                cur.execute(
                    "UPDATE tb SET Password = {} "
                    "WHERE Email like {};".format(newPassword, emailAddress)
                )
                conn.commit()
                self.ui.labelResponse.setText("Password successfully changed.")
            else:
                self.ui.labelResponse.setText("No record found.")

        except:
            self.ui.labelResponse.setText("Error.")
        finally:
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
