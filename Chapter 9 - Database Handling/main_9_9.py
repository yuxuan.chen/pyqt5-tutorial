from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3, sys
from demo_9_9 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.labelSure.hide()
        self.ui.pushButtonYes.hide()
        self.ui.pushButtonNo.hide()
        self.ui.pushButtonDelete.clicked.connect(self.deleteRow)
        self.ui.pushButtonYes.clicked.connect(self.confirmDelete)
        self.ui.pushButtonNo.clicked.connect(self.rejectDelete)

        self.show()

    def deleteRow(self):
        emailAddress = self.ui.lineEditEmailAddress.text()
        password = self.ui.lineEditPassword.text()

        try:
            conn = sqlite3.connect("db.db")
            cur = conn.cursor()
            cur.execute(
                "SELECT * FROM tb "
                "WHERE Email like {} AND Password like {};".format(
                    emailAddress, password
                )
            )
            row = cur.fetchone()
            if row:
                self.ui.labelSure.show()
                self.ui.pushButtonYes.show()
                self.ui.pushButtonNo.show()
                self.ui.labelResponse.setText("")
            else:
                self.ui.labelSure.hide()
                self.ui.pushButtonYes.hide()
                self.ui.pushButtonNo.hide()
                self.ui.labelResponse.setText(
                    "Sorry, incorrect email address or password."
                )
        except:
            self.ui.labelResponse.setText("Error in searching.")
        finally:
            conn.close()

    def confirmDelete(self):
        emailAddress = self.ui.lineEditEmailAddress.text()
        password = self.ui.lineEditPassword.text()

        try:
            conn = sqlite3.connect("db.db")
            cur = conn.cursor()
            cur.execute(
                "DELETE FROM tb "
                "WHERE Email like {} AND Password like {};".format(
                    emailAddress, password
                )
            )
            conn.commit()

            self.ui.labelResponse.setText("Successfully deleted.")
            self.ui.labelSure.hide()
            self.ui.pushButtonYes.hide()
            self.ui.pushButtonNo.hide()

        except:
            self.ui.labelResponse.setText("Error in deleting")
        finally:
            conn.close()

    def rejectDelete(self):
        self.ui.labelSure.hide()
        self.ui.pushButtonYes.hide()
        self.ui.pushButtonNo.hide()
        self.ui.labelResponse.setText("Canceled by user.")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
