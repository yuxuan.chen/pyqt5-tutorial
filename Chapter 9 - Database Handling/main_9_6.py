from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
import sys
from demo_9_6 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonSearch.clicked.connect(self.search)
        self.show()

    def search(self):
        databaseName = self.ui.lineEditDBName.text()
        tableName = self.ui.lineEditTableName.text()
        email = self.ui.lineEditEmailAddress.text()
        query = 'SELECT Password FROM {} where Email like "{}";'.format(
            tableName, email
        )

        try:
            conn = sqlite3.connect(databaseName + ".db")
            cur = conn.cursor()
            cur.execute(query)
            row = cur.fetchone()
            if row:
                self.ui.labelResponse.setText("Email address found.")
                self.ui.lineEditPassword.setText(row[0])
            else:
                self.ui.labelResponse.setText("Sorry, no record found.")
                self.ui.lineEditPassword.setText("")
        except:
            self.ui.labelResponse.setText("Error.")
        finally:
            conn.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
