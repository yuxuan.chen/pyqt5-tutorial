from PyQt5.QtWidgets import QDialog, QApplication
import sys, time
from threading import Thread, Lock
from demo_8_5 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.show()


class MyThread(Thread):
    def __init__(self, progressBar):
        Thread.__init__(self)
        self.counter = 0
        self.progressBar = progressBar

    def run(self):
        print("Starting {}".format(self.name))
        with threadLock:
            while self.counter <= 100:
                time.sleep(0.5)
                self.progressBar.setValue(self.counter)
                self.counter += 10
                print("Exiting {}".format(self.name))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    thread1 = MyThread(w.ui.progressBarDownload)
    thread2 = MyThread(w.ui.progressBarScan)
    threadLock = Lock()

    thread1.start()
    thread2.start()

    sys.exit(app.exec_())
