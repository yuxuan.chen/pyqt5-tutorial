from PyQt5.QtWidgets import QDialog, QApplication
from quamash import QEventLoop
import sys, time, asyncio
from threading import Thread, Lock
from demo_8_4 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonStart.clicked.connect(self.invokeAsync)
        self.show()

    def invokeAsync(self):
        asyncio.ensure_future(self.update(1, self.ui.progressBarDownload))
        asyncio.ensure_future(self.update(2, self.ui.progressBarScan))


@staticmethod
async def update(delay, progressBar):
    for i in range(101):
        await asyncio.sleep(delay)
        progressBar.setValue(i)


class MyThread(Thread):
    def __init__(self, progressBar):
        Thread.__init__(self)
        self.counter = 0
        self.progressBar = progressBar

    def run(self):
        print("Starting {}".format(self.name))
        lock.acquire()
        while self.counter <= 100:
            time.sleep(0.5)
            self.progressBar.setValue(self.counter)
            self.counter += 10
            print("Exiting {}".format(self.name))
        lock.release()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)
    with loop:
        loop.run_forever()
        loop.close()

    sys.exit(app.exec_())
