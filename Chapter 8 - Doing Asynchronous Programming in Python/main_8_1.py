from PyQt5.QtWidgets import QDialog, QApplication
import sys, time
from threading import Thread
from demo_8_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.show()


class MyThread(Thread):
    def __init__(self, w):
        Thread.__init__(self)
        self.w = w
        self.counter = 0

    def run(self):
        print("Starting {}".format(self.name))
        while self.counter <= 100:
            time.sleep(0.5)
            w.ui.progressBar.setValue(self.counter)
            self.counter += 10
            print("Exiting {}".format(self.name))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    thread = MyThread(w)
    thread.start()
    sys.exit(app.exec_())
