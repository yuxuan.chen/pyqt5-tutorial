from PyQt5.QtWidgets import QDialog, QApplication
import googlemaps
import sys
from demo_12_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonSearch.clicked.connect(self.displayDetails)
        self.show()

    def displayDetails(self):
        address = self.ui.lineEditLocation.text()
        googleMaps = googlemaps.Client(key="AIzaSyDwJg3_iAwn4JYd8box6EPAtta2icvRaas")
        result = googleMaps.geocode(address)

        self.ui.labelCity.setText("City: {}".format(result.city))
        self.ui.labelPostalCode.setText("Postal Code: {}".format(result.postal_code))
        self.ui.labelLongtitude.setText("Longitude: {}".format(result.lng))
        self.ui.labelLatitude.setText("Latitude: {}".format(result.lat))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
