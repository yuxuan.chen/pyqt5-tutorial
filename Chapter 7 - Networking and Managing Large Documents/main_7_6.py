from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QPainter
import sys
from demo_7_6 import *


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.pos1, self.pos2 = [0, 0], [0, 0]
        self.toDraw = ""

        self.ui.actionDraw_Circle.triggered.connect(self.drawCircle)
        self.ui.actionDraw_Rectangle.triggered.connect(self.drawRectangle)
        self.ui.actionDraw_Line.triggered.connect(self.drawLine)
        self.ui.actionPage_Setup.triggered.connect(self.pageSetup)
        self.ui.actionSet_Password.triggered.connect(self.setPassword)
        self.ui.actionCut.triggered.connect(self.cut)
        self.ui.actionCopy.triggered.connect(self.copy)
        self.ui.actionPaste.triggered.connect(self.paste)

        self.show()

    def paint(self, event):
        qp = QPainter()
        qp.begin(self)

        if self.toDraw == "cirle":
            width = self.pos2[0] - self.pos1[0]
            height = self.pos2[1] - self.pos1[1]
            rect = QtCore.QRect(self.pos1[0], self.pos1[1], width, height)
            qp.drawArc(rect, startAngle=0, arcLength=360 * 16)
        elif self.toDraw == "rectangle":
            width = self.pos2[0] - self.pos1[0]
            height = self.pos2[1] - self.pos1[1]
            rect = QtCore.QRect(self.pos1[0], self.pos1[1], width, height)
        elif self.toDraw == "line":
            qp.drawLine(self.pos1[0], self.pos1[1], self.pos2[0], self.pos2[1])
        qp.end()

    def drawCircle(self):
        self.ui.label.setText("")
        self.toDraw = "circle"

    def drawRectangle(self):
        self.ui.label.setText("")
        self.toDraw = "rectangle"

    def drawLine(self):
        self.ui.label.setText("")
        self.toDraw = "line"

    def pageSetup(self):
        self.ui.label.setText("Page setup")

    def setPassword(self):
        self.ui.label.setText("Set password")

    def cut(self):
        self.ui.label.setText("Cut")

    def copy(self):
        self.ui.label.setText("Copy")

    def paste(self):
        self.ui.label.setText("Paste")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())
