from PyQt5.QtWidgets import QMainWindow, QApplication
import sys
from demo_7_4 import *


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.mdiArea.addSubWindow(self.ui.subwindow)
        self.ui.mdiArea.addSubWindow(self.ui.subwindow_2)

        self.ui.actionSubWindowView.triggered.connect(self.subWindowView)
        self.ui.actionTabbedView.triggered.connect(self.tabbedView)
        self.ui.actionCascadeView.triggered.connect(self.cascadeView)
        self.ui.actionTileView.triggered.connect(self.tileView)

        self.show()

    def subWindowView(self):
        self.ui.mdiArea.setViewMode(0)

    def tabbedView(self):
        self.ui.mdiArea.setViewMode(1)

    def cascadeView(self):
        self.ui.mdiArea.cascadeSubWindows()

    def tileView(self):
        self.ui.mdiArea.tileSubWindows()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())
