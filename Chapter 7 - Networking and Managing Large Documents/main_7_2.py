from PyQt5.QtWidgets import QDialog, QApplication
import sys, time
import socket
from threading import Thread
from sockerserver import ThreadingMixIn
from demo_7_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButtonSend.clicked.connect(self.disMsg)
        self.show()

    def disMsg(self):
        text = self.ui.textEditMsg.text()
        global connection
        connection.send(text.encode("utf-8"))

        self.ui.textEditMsg.append("Server: {}".format(self.ui.lineEditMsg.text()))
        self.ui.lineEditMsg.setText("")


class ServerThread(Thread):
    def __init__(self, window):
        Thread.__init__(self)
        self.window = window

    def run(self):
        TCP_IP, TCP_PORT = "0.0.0.0", 80
        BUFFER_SIZE = 1024

        # configurate TCP server
        tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcpServer.bind((TCP_IP, TCP_PORT))
        tcpServer.listen(4)

        threads = []
        while True:
            global connection
            (connection, (ip, port)) = tcpServer.accept()
            newThread = ClientThread(ip, port, window)
            newThread.start()
            threads.append(newThread)

        for thread in threads:
            thread.join()


class ClientThread(Thread):
    def __init__(self, ip="0,0,0,0", port=0, window=None):
        Thread.__init__(self)
        self.window = window
        self.ip = ip
        self.port = port

    def run(self):
        BUFFER_SIZE = 1024
        while True:
            global connection
            data = connection.recv(BUFFER_SIZE)
            window.ui.textEditMsg.append("Client: {}".format(data.encode("utf-8")))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    serverThread = ServerThread(window)
    serverThread.start()
    w.exec_()
    sys.exit(app.exec_())
