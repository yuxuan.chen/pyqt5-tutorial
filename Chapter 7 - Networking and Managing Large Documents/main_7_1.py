from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtCore import QUrl
import sys
from demo_7_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonGo.clicked.connect(self.dispSite)

        self.show()

    def dispSite(self):
        url = QUrl(self.ui.lineEditURL.text())
        self.ui.webEngineView.load(url)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
