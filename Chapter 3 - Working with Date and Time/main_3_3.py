from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_3_3 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        # initialization
        self.roomTypes = ["Suite", "Super Luxury", "Super Deluxe", "Ordinary"]
        self.priceList = {
            "Suite": 40,
            "Super Luxury": 30,
            "Super Deluxe": 20,
            "Ordinary": 10,
        }
        self.ui.comboBox.addItems(self.roomTypes)

        self.ui.pushButtonCalculate.clicked.connect(self.dispInfo)

        self.show()

    def dispInfo(self):
        # get starting date
        date = self.ui.calendarWidget.selectedDate()
        date_formatted = str(date.toPyDate())
        # get number of duration
        numberDays = self.ui.spinBoxNumberDays.value()
        # get room type
        roomType = self.ui.comboBox.itemText(self.ui.comboBox.currentIndex())

        # display time info
        self.ui.labelTime.setText(
            "Start date: {}, {} days, Room type: {}".format(
                date_formatted, numberDays, roomType
            )
        )

        # display rent info
        rent = numberDays * self.priceList[roomType]
        self.ui.labelRent.setText("Total rent is {}".format(rent))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
