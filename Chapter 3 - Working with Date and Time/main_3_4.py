from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem
import sys
from demo_3_4 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.priceList = [
            ("Suite", 40),
            ("Super Luxury", 30),
            ("Super Deluxe", 20),
            ("Ordinary", 10),
        ]

        # row = 0
        # for tup in self.priceList:
        #     col = 0
        #     for item in tup:
        #         item = QTableWidgetItem(item)
        #         self.ui.tableWidget.setItem(row, col, item)
        #         col += 1
        #     row += 1
        for row in range(4):
            for col in range(2):
                item = QTableWidgetItem(self.priceList[row][col])
                self.ui.tableWidget.setItem(row, col, item)

        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
