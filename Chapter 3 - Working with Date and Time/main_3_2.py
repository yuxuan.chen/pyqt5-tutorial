from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_3_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.calendarWidget.selectionChanged.connect(self.dispDate)

        self.show()

    def dispDate(self):
        self.ui.dateEdit.setDate(self.ui.calendarWidget.selectedDate())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
