from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_3_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.showLCD)
        # set the timer to generate a timeout() signal every 1000 ms
        timer.start(1000)
        self.showLCD()

        self.show()

    def showLCD(self):
        curr_time = QtCore.QTime.currentTime()
        curr_time_formatted = curr_time.toString("hh:mm")
        self.ui.lcdNumber.display(curr_time_formatted)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
