from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_4_1 import *


class Student:
    def __init__(self, name=""):
        self.name = name

    def getName(self):
        return self.name


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonClickMe.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):
        student = Student(self.ui.lineEditUsername.text())
        self.ui.labelResponse.setText("Hello {}".format(student.getName()))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
