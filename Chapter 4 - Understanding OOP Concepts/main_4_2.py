from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_4_2 import *


class Student:
    def __init__(self, name="", ID=0):
        self.name = name
        self.ID = ID

    def getName(self):
        return self.name

    def getID(self):
        return self.ID


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonClickMe.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):
        name = self.ui.lineEditName.text()
        ID = self.ui.lineEditID.text()
        student = Student(name, ID)

        self.ui.labelID.setText("ID: {}".format(student.getID()))
        self.ui.labelName.setText("Name: {}".format(student.getName()))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
