from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_4_5 import *


class Student:
    def __init__(self, name="", ID=0):
        self.name = name
        self.ID = ID

    def getName(self):
        return self.name

    def getID(self):
        return self.ID


class Marks:
    def __init__(self, historyMarks=0, geographyMarks=0):
        self.historyMarks = int(historyMarks)
        self.geographyMarks = int(geographyMarks)

    def getHistoryMarks(self):
        return self.historyMarks

    def getGeographyMarks(self):
        return self.geographyMarks


class Result(Student, Marks):
    def __init__(self, name="", ID=0, historyMarks=0, geographyMarks=0):
        Student.__init__(self, name, ID)
        Marks.__init__(self, historyMarks, geographyMarks)
        self.totalMarks = self.historyMarks + self.geographyMarks
        self.percentage = self.totalMarks / 200

    def getTotalMarks(self):
        return self.totalMarks

    def getPercentage(self):
        return self.percentage


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonClickMe.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):
        result = Result(
            name=self.ui.lineEditName.text(),
            ID=self.ui.lineEditID.text(),
            historyMarks=self.ui.lineEditHistoryMarks.text(),
            geographyMarks=self.ui.lineEditGeographyMarks.text(),
        )
        self.ui.lineEditTotal.setText(str(result.getTotalMarks()))
        self.ui.lineEditPercentage.setText("{}%".format(result.getPercentage() * 100))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
