from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_4_3 import *


class Student:
    def __init__(self, name="", ID=0):
        self.name = name
        self.ID = ID

    def getName(self):
        return self.name

    def getID(self):
        return self.ID


class Marks(Student):
    def __init__(self, name="", ID=0, historyMarks=0, geographyMarks=0):
        Student.__init__(self, name=name, ID=ID)
        self.historyMarks = int(historyMarks)
        self.geographyMarks = int(geographyMarks)

    def getHistoryMarks(self):
        return self.historyMarks

    def getGeographyMarks(self):
        return self.geographyMarks


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonClickMe.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):
        marksObj = Marks(
            name=self.ui.lineEditName.text(),
            ID=self.ui.lineEditID.text(),
            historyMarks=self.ui.lineEditHistoryMarks.text(),
            geographyMarks=self.ui.lineEditGeographyMarks.text(),
        )

        self.ui.label_Response.setText(
            "ID: {}, Name: {}, Totol marks: {}".format(
                marksObj.getID(),
                marksObj.getName(),
                marksObj.getHistoryMarks() + marksObj.getGeographyMarks(),
            )
        )


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
