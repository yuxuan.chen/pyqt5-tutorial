# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '4_1_use_classes_in_GUI.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(413, 199)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 20, 141, 24))
        self.label.setObjectName("label")
        self.lineEditUsername = QtWidgets.QLineEdit(Dialog)
        self.lineEditUsername.setGeometry(QtCore.QRect(170, 10, 231, 38))
        self.lineEditUsername.setObjectName("lineEditUsername")
        self.labelResponse = QtWidgets.QLabel(Dialog)
        self.labelResponse.setGeometry(QtCore.QRect(20, 90, 371, 24))
        self.labelResponse.setText("")
        self.labelResponse.setObjectName("labelResponse")
        self.pushButtonClickMe = QtWidgets.QPushButton(Dialog)
        self.pushButtonClickMe.setGeometry(QtCore.QRect(160, 150, 110, 40))
        self.pushButtonClickMe.setObjectName("pushButtonClickMe")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Enter your name"))
        self.pushButtonClickMe.setText(_translate("Dialog", "Click"))
