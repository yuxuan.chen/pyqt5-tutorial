from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_1_4 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.checkBoxCheese.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxOlives.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxSausages.stateChanged.connect(self.dispAmount)

        self.show()

    def dispAmount(self):
        amount = 10
        if self.ui.checkBoxCheese.isChecked():
            amount += 1
        if self.ui.checkBoxOlives.isChecked():
            amount += 1
        if self.ui.checkBoxSausages.isChecked():
            amount += 2

        self.ui.labelAmount.setText("Total amount for your pizza is {}".format(amount))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
