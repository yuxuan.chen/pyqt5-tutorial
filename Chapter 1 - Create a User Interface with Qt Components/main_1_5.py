from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_1_5 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.checkBoxChocolateChips.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxCookieDough.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxChocolateAlmond.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxRockyRoad.stateChanged.connect(self.dispAmount)

        self.ui.checkBoxCoffee.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxSoda.stateChanged.connect(self.dispAmount)
        self.ui.checkBoxTee.stateChanged.connect(self.dispAmount)

        self.show()

    def dispAmount(self):
        amount = 0
        if self.ui.checkBoxChocolateChips.isChecked():
            amount += 4
        if self.ui.checkBoxCookieDough.isChecked():
            amount += 2
        if self.ui.checkBoxChocolateAlmond.isChecked():
            amount += 3
        if self.ui.checkBoxRockyRoad.isChecked():
            amount += 5

        if self.ui.checkBoxCoffee.isChecked():
            amount += 2
        if self.ui.checkBoxSoda.isChecked():
            amount += 3
        if self.ui.checkBoxTee.isChecked():
            amount += 1

        self.ui.labelAmount.setText("Total amount is {}".format(amount))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
