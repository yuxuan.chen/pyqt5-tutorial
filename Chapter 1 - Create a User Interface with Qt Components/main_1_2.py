from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_1_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.radioButtonFirstClass.toggled.connect(self.dispFare)
        self.ui.radioButtonBusinessClass.toggled.connect(self.dispFare)
        self.ui.radioButtonEconomyClass.toggled.connect(self.dispFare)

        self.show()

    def dispFare(self):
        fare = 0
        if self.ui.radioButtonFirstClass.isChecked():
            fare = 150
        elif self.ui.radioButtonBusinessClass.isChecked():
            fare = 125
        elif self.ui.radioButtonEconomyClass.isChecked():
            fare = 100
        self.ui.labelFare.setText("Air Fare is {}".format(fare))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
