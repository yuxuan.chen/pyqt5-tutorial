from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_1_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(myForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.buttonClickMe.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):
        self.ui.labelResponse.setText("Hello " + self.ui.lineEditName.text())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
