from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonPlus.clicked.connect(self.add)
        self.ui.pushButtonMinus.clicked.connect(self.subtract)
        self.ui.pushButtonMultiply.clicked.connect(self.multiply)
        self.ui.pushButtonDivide.clicked.connect(self.divide)

        self.show()

    def add(self):
        a = self.ui.lineEditFirstNumber.text()
        b = self.ui.lineEditSecondNumber.text()
        a = int(a) if a else 0
        b = int(b) if b else 0
        self.ui.labelResult.setText("Addition: {}".format(a + b))

    def subtract(self):
        a = self.ui.lineEditFirstNumber.text()
        b = self.ui.lineEditSecondNumber.text()
        a = int(a) if a else 0
        b = int(b) if b else 0
        self.ui.labelResult.setText("Subtraction: {}".format(a - b))

    def multiply(self):
        a = self.ui.lineEditFirstNumber.text()
        b = self.ui.lineEditSecondNumber.text()
        a = int(a) if a else 0
        b = int(b) if b else 0
        self.ui.labelResult.setText("Multiplication: {}".format(a * b))

    def divide(self):
        a = self.ui.lineEditFirstNumber.text()
        b = self.ui.lineEditSecondNumber.text()
        a = int(a) if a else 0
        b = int(b) if b else 0
        result = round(a / b, 2) if b else "NA"
        self.ui.labelResult.setText("Addition: {}".format(result))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
