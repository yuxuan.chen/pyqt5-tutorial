from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_11 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonStart.clicked.connect(self.startDownloaded)

        self.show()

    def startDownloaded(self):
        x = 0
        while x < 100:
            x += 0.0001
            self.ui.progressBar.setValue(x)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
