from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_3 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.spinBoxBookQty.editingFinished.connect(self.proc_book)
        self.ui.doubleSpinBoxSugarWeight.editingFinished.connect(self.proc_sugar)

        self.show()

    def proc_book(self):
        bookPrice = self.ui.lineEditBookPrice.text()
        bookPrice = int(bookPrice) if bookPrice else 0
        bookAmount = self.ui.spinBoxBookQty.value() * bookPrice
        self.ui.lineEditBookAmount.setText(str(bookAmount))

        # update total
        sugarAmount = self.ui.lineEditSugarAmount.text()
        sugarAmount = double(sugarAmount) if sugarAmount else 0
        totalAmount = bookAmount + sugarAmount
        self.ui.labelTotalAmount.setText(str(totalAmount))

    def proc_sugar(self):
        sugarPrice = self.ui.lineEditSugarPrice.text()
        sugarPrice = float(sugarPrice) if sugarPrice else 0
        sugarAmount = self.ui.doubleSpinBoxSugarWeight.value() * sugarPrice
        sugarAmount = round(sugarAmount, 2)
        self.ui.lineEditSugarAmount.setText(str(sugarAmount))

        # update total
        bookAmount = self.ui.lineEditBookAmount.text()
        bookAmount = int(bookAmount) if bookAmount else 0
        totalAmount = bookAmount + sugarAmount
        self.ui.labelTotalAmount.setText(str(totalAmount))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
