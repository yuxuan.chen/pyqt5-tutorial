from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_6 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.listWidgetDiagnosis.itemSelectionChanged.connect(self.dispSelectedTests)

        self.show()

    def dispSelectedTests(self):
        self.ui.listWidgetSelectedTests.clear()

        items = self.ui.listWidgetDiagnosis.selectedItems()
        for item in items:
            self.ui.listWidgetSelectedTests.addItem(item.text())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
