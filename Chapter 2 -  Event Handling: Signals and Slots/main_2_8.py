from PyQt5.QtWidgets import QDialog, QApplication, QInputDialog, QListWidgetItem
import sys
from demo_2_8 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        # initialization
        self.ui.listWidget.addItems(["Ice Cream", "Soda", "Coffee"])

        self.ui.pushButtonAdd.clicked.connect(self.addToList)
        self.ui.pushButtonEdit.clicked.connect(self.edit)
        self.ui.pushButtonDelete.clicked.connect(self.delete)
        self.ui.pushButtonDeleteAll.clicked.connect(self.deleteAll)

        self.show()

    def addToList(self):
        item = self.ui.lineEditItem.text()
        if item:
            self.ui.listWidget.addItem(item)
        self.ui.lineEditItem.setText("")
        self.ui.lineEditItem.setFocus()

    def edit(self):
        curr = self.ui.listWidget.currentRow()
        newText, success = QInputDialog.getText(self, "Edit", "Enter new text")
        if success and newText:
            self.ui.listWidget.takeItem(curr)
            self.ui.listWidget.insertItem(curr, QListWidgetItem(newText))

    def delete(self):
        self.ui.listWidget.takeItem(self.ui.listWidget.currentRow())

    def deleteAll(self):
        self.ui.listWidget.clear()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
