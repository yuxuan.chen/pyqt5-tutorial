from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_9 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        # initialization(set default)
        self.ui.labelAccountType.setText("You have selected Saving Account")

        self.ui.comboBoxAccountType.currentIndexChanged.connect(self.dispAccountType)

        self.show()

    def dispAccountType(self):
        self.ui.labelAccountType.setText(
            "You have selected {}".format(self.ui.comboBoxAccountType.currentText())
        )


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
