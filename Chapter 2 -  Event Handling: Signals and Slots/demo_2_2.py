# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '2_2_convert_data_types_and_make_a_calculatot.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.labelFirstNumber = QtWidgets.QLabel(Dialog)
        self.labelFirstNumber.setGeometry(QtCore.QRect(20, 30, 161, 24))
        self.labelFirstNumber.setObjectName("labelFirstNumber")
        self.labelSecondNumber = QtWidgets.QLabel(Dialog)
        self.labelSecondNumber.setGeometry(QtCore.QRect(20, 70, 181, 24))
        self.labelSecondNumber.setObjectName("labelSecondNumber")
        self.labelResult = QtWidgets.QLabel(Dialog)
        self.labelResult.setGeometry(QtCore.QRect(50, 240, 311, 24))
        self.labelResult.setText("")
        self.labelResult.setObjectName("labelResult")
        self.lineEditFirstNumber = QtWidgets.QLineEdit(Dialog)
        self.lineEditFirstNumber.setGeometry(QtCore.QRect(220, 20, 151, 38))
        self.lineEditFirstNumber.setObjectName("lineEditFirstNumber")
        self.lineEditSecondNumber = QtWidgets.QLineEdit(Dialog)
        self.lineEditSecondNumber.setGeometry(QtCore.QRect(220, 70, 151, 38))
        self.lineEditSecondNumber.setObjectName("lineEditSecondNumber")
        self.pushButtonPlus = QtWidgets.QPushButton(Dialog)
        self.pushButtonPlus.setGeometry(QtCore.QRect(30, 160, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.pushButtonPlus.setFont(font)
        self.pushButtonPlus.setObjectName("pushButtonPlus")
        self.pushButtonMinus = QtWidgets.QPushButton(Dialog)
        self.pushButtonMinus.setGeometry(QtCore.QRect(120, 160, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.pushButtonMinus.setFont(font)
        self.pushButtonMinus.setObjectName("pushButtonMinus")
        self.pushButtonMultiply = QtWidgets.QPushButton(Dialog)
        self.pushButtonMultiply.setGeometry(QtCore.QRect(210, 160, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.pushButtonMultiply.setFont(font)
        self.pushButtonMultiply.setObjectName("pushButtonMultiply")
        self.pushButtonDivide = QtWidgets.QPushButton(Dialog)
        self.pushButtonDivide.setGeometry(QtCore.QRect(300, 160, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.pushButtonDivide.setFont(font)
        self.pushButtonDivide.setObjectName("pushButtonDivide")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.labelFirstNumber.setText(_translate("Dialog", "Enter first number"))
        self.labelSecondNumber.setText(_translate("Dialog", "Enter second number"))
        self.pushButtonPlus.setText(_translate("Dialog", "+"))
        self.pushButtonMinus.setText(_translate("Dialog", "-"))
        self.pushButtonMultiply.setText(_translate("Dialog", "x"))
        self.pushButtonDivide.setText(_translate("Dialog", "/"))
