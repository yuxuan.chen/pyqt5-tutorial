from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_4 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.horizontalScrollBarSugarLevel.valueChanged.connect(
            self.proc_sugar_level
        )
        self.ui.verticalScrollBarPulseRate.valueChanged.connect(self.proc_pulse_rate)
        self.ui.horizontalSliderBloodPressure.valueChanged.connect(
            self.proc_blood_pressure
        )
        self.ui.verticalSliderCholestrolLevel.valueChanged.connect(
            self.proc_cholestrol_level
        )

        self.show()

    def proc_sugar_level(self, value):
        # value = self.ui.horizontalScrollBarSugarLevel.value()
        self.ui.lineEditResult.setText("Sugar Level: {}".format(value))

    def proc_pulse_rate(self, value):
        # value = self.ui.verticalScrollBarPulseRate.value()
        self.ui.lineEditResult.setText("Pulse Rate: {}".format(value))

    def proc_blood_pressure(self, value):
        # value = self.ui.horizontalSliderBloodPressure.value()
        self.ui.lineEditResult.setText("Blood Pressure: {}".format(value))

    def proc_cholestrol_level(self, value):
        # value = self.ui.verticalSliderCholestrolLevel.value()
        self.ui.lineEditResult.setText("Cholestrol Level: {}".format(value))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
