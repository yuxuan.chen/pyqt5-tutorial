from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_7 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonAdd.clicked.connect(self.addToList)

        self.show()

    def addToList(self):
        item = self.ui.lineEditFoodItem.text()
        if item:
            self.ui.listWidgetFoodItems.addItem(item)

        # reset
        self.ui.lineEditFoodItem.setText("")
        self.ui.lineEditFoodItem.setFocus()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
