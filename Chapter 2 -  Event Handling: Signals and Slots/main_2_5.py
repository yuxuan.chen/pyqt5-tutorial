from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_5 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.listWidgetDisgnosis.itemClicked.connect(self.dispSelectedTest)

        self.show()

    def dispSelectedTest(self):
        result = self.ui.listWidgetDisgnosis.currentItem().text()
        self.ui.labelTest.setText("You have selected {}".format(result))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
