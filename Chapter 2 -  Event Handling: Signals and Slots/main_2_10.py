from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_2_10 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.fontComboBox.currentFontChanged.connect(self.changeFont)
        self.ui.horizontalSliderFontSize.valueChanged.connect(self.changeFont)

        self.show()

    def changeFont(self):
        fontName = self.ui.fontComboBox.itemText(self.ui.fontComboBox.currentIndex())
        fontSize = self.ui.horizontalSliderFontSize.value()
        currFont = QtGui.QFont(fontName, fontSize)
        self.ui.textEdit.setFont(currFont)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
