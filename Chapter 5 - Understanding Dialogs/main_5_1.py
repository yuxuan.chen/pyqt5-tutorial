from PyQt5.QtWidgets import QDialog, QApplication, QInputDialog
import sys
from demo_5_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.countries = ("Argentina", "Australia", "Austria")

        self.ui.pushButtonCountry.clicked.connect(self.dispmessage)

        self.show()

    def dispmessage(self):

        countryName, success = QInputDialog.getItem(
            self, "Input Dialog", "List of countries", self.countries, 0, False
        )
        if success and countryName:
            self.ui.lineEditCountry.setText(countryName)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
