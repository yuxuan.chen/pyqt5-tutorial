from PyQt5.QtWidgets import QDialog, QApplication, QFontDialog
import sys
from demo_5_3 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonFont.clicked.connect(self.changeFont)

        self.show()

    def changeFont(self):
        font, success = QFontDialog.getFont()
        if success:
            self.ui.textEdit.setFont(font)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
