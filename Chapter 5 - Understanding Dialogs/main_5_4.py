from PyQt5.QtWidgets import QDialog, QApplication, QMainWindow, QAction, QFileDialog
import sys
from demo_5_4 import *


class MyForm(QMainWindow):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.actionOpen.triggered.connect(self.openFile)
        self.ui.actionSave.triggered.connect(self.saveFile)

        self.show()

    def openFile(self):
        fname, _ = QFileDialog.getOpenFileName(self, "Open file", "./")
        if fname:
            with open(fname, "r") as f:
                text = f.read()
                self.ui.textEdit.setText(text)

    def saveFile(self):
        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        fname, _ = QFileDialog.getSaveFileName(
            self,
            "QFileDialog.getSaverFileName()",
            "",
            "All Files (*);;Text Files (*.txt)",
            options=options,
        )

        with open(fname, "w") as f:
            text = self.ui.textEdit.toPlainText()
            f.write(text)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
