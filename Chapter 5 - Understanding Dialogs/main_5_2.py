from PyQt5.QtWidgets import QDialog, QApplication, QColorDialog
from PyQt5.QtGui import QColor
import sys
from demo_5_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        color = QColor(0, 0, 0)
        self.ui.frameColor.setStyleSheet(
            "QWidget { background-color: %s }" % color.name()
        )
        self.ui.pushButtonColor.clicked.connect(self.dispcolor)

        self.show()

    def dispcolor(self):
        color = QColorDialog.getColor()
        if color.isValid():
            self.ui.frameColor.setStyleSheet(
                "QWidget { background-color: %s }" % color.name()
            )


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
