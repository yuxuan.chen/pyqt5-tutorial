from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtGui import QPainter, QColor, QFont
import PyQt5.QtCore
import sys
from demo_10_8 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.pushButtonDrawText.clicked.connect(self.drawText)
        self.text = ""
        self.font, self.size = "Courier New", 10

        self.show()

    def drawText(self):
        self.text = self.ui.textEdit.toPlainText()
        self.font = self.ui.listWidgetFont.currentItem().text()
        self.size = int(
            self.ui.comboBoxSize.itemText(self.ui.comboBoxSize.currentIndex())
        )
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        qp.setPen(QColor(168, 34, 3))
        qp.setFont(QFont(self.font, self.size))
        qp.drawText(event.rect(), QtCore.Qt.AlignCenter, self.text)

        qp.end()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
