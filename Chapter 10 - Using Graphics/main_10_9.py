from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QPainter
from PyQt5 import QtCore
import sys
from demo_10_9 import *


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.pos1, self.pos2 = [0, 0], [0, 0]
        self.toDraw = ""
        self.ui.actionCircle.triggered.connect(self.drawCircle)
        self.ui.actionLine.triggered.connect(self.drawLine)
        self.ui.actionRectangle.triggered.connect(self.drawRectangle)

        self.show()

    def drawCircle(self):
        self.toDraw = "circle"

    def drawLine(self):
        self.toDraw = "line"

    def drawRectangle(self):
        self.toDraw = "rectangle"

    def MousePressEvent(self, event):
        if event.button() and QtCore.Qt.LeftButton:
            self.pos1 = [event.pos().x(), event.pos().y()]

    def MouseReleaseEvent(self, event):
        self.pos2 = [event.pos().x(), event.pos().y()]
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        if self.toDraw == "circle":
            width = self.pos2[0] - self.pos1[0]
            height = self.pos2[1] - self.pos1[1]
            startAngle = 0
            arcLength = 360 * 16
            rect = QtCore.QRect(self.pos1[0], self.pos1[1], width, height)
            qp.drawArc(rect, startAngle, arcLength)
        elif self.toDraw == "line":
            qp.drawLine(self.pos1[0], self.pos1[1], self.pos2[0], self.pos2[1])
        elif self.toDraw == "rectangle":
            width = self.pos2[0] - self.pos1[0]
            height = self.pos2[1] - self.pos1[1]
            qp.drawRect(self.pos1[0], self.pos1[1], width, height)
        qp.end()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyWindow()
    w.show()
    sys.exit(app.exec_())
