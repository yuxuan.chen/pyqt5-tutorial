from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtGui import QPainter
import PyQt5.QtCore
import sys
from demo_10_7 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.pos1, self.pos2 = [0, 0], [0, 0]
        self.show()

    def MousePressEvent(self, event):
        if event.button() and QtCore.Qt.LeftButton:
            self.pos1 = [event.pos().x(), event.pos().y()]

    def MouseReleaseEvent(self, event):
        self.pos2 = [event.pos().x(), event.pos().y()]
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        width = self.pos2[0] - self.pos1[0]
        height = self.pos2[1] - self.pos1[1]
        qp.drawrect(self.pos1[0], self.pos2[0], width, height)
        qp.end()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
