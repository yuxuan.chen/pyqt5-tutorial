from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtGui import QPainter, QPen
import PyQt5.QtCore
import sys
from demo_10_5 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.lineType = "SolidLine"
        self.pos1, self.pos2 = [0, 0], [0, 0]

        self.show()

    def MousePressEvent(self, event):
        if event.button() and QtCore.Qt.LeftButton:
            self.pos1 = [event.pos().x(), event.pos().y()]

    def MouseReleaseEvent(self, event):
        self.lineType = self.ui.listWidgetLineType.currentItem().text()
        self.pos2 = [event.pos().x(), event.pos().y()]
        self.update()

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)

        pen = QPen(QtCore.Qt.black, 4)
        if self.lineType == "SolidLine":
            pen.setStyle(QtCore.Qt.SolidLine)
        elif self.lineType == "DashLine":
            pen.setStyle(QtCore.Qt.DashLine)
        elif self.lineType == "DashDotLine":
            pen.setStyle(QtCore.Qt.DashDotLine)
        elif self.lineType == "DotLine":
            pen.setStyle(QtCore.Qt.DotLine)
        elif self.lineType == "DashDotDotLine":
            pen.setStyle(QtCore.Qt.DashDotDotLine)

        qp.setPen(pen)
        qp.drawLine(self.pos1[0], self.pos1[1], self.pos2[0], self.pos2[1])
        qp.end()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
