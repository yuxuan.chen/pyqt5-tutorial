from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_10_1 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.setMouseTracking(True)
        self.ui.setupUi(self)
        self.show()

    def mouseMoveEvent(self, event):
        self.ui.labelCoordinate.setText("x:{}, y:{}".format(event.x(), event.y()))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
