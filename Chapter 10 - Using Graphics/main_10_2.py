from PyQt5.QtWidgets import QDialog, QApplication
import sys
from demo_10_2 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.setMouseTracking(True)
        self.ui.setupUi(self)
        self.show()

    def mousePressEvent(self, event):
        self.ui.labelClickCoordinate.setText(
            "Click coordinate: x:{}, y:{}".format(event.x(), event.y())
        )

    def mouseReleaseEvent(self, event):
        self.ui.labelReleaseCoordinate.setText(
            "Release coordinate: x:{}, y:{}".format(event.x(), event.y())
        )


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
