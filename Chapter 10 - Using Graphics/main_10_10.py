from PyQt5.QtWidgets import QDialog, QApplication
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas
from numpy import linspace
import sys
from demo_10_10 import *


class MyForm(QDialog):
    def __init__(self):
        super(MyForm, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.a, self.b = 0, 0
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)

        self.ui.verticalLayoutOutput.addWidget(self.canvas)
        self.ui.pushButtonPlot.clicked.connect(self.plot)

        self.show()

    def plot(self):
        a, b = float(self.ui.lineEditA.text()), float(self.ui.lineEditB.text())
        x = linspace(0, 5)
        y = a * x + b

        self.figure.clear()
        ax = self.figure.add_subplot(111)
        ax.plot(x, y)
        self.canvas.draw()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
